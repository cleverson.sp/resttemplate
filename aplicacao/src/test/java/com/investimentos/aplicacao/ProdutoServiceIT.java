package com.investimentos.aplicacao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProdutoServiceIT {
	
	@Autowired
	private ProdutoService subject;
	
	@Test
	public void deveBuscarUmProduto() {
		Optional<Produto> produto = subject.consultar(111);
		
		assertTrue(produto.isPresent());
		assertNotNull(produto);
	}

}
