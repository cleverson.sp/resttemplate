package com.investimentos.aplicacao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ClienteServiceIT {
	
	@Autowired
	private ClienteService subject;
	
	@Test
	public void deveBuscarUmClientePorCPF() {
		Optional<Cliente> cliente = subject.consultarCPFCliente("30892029803");
		
		assertTrue(cliente.isPresent());
		assertNotNull(cliente);
	}

}
