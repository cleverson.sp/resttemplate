package com.investimentos.aplicacao;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class ClienteService {

	private RestTemplate restTemplate = new RestTemplate();
	private Logger logger = LoggerFactory.getLogger(ProdutoService.class);
	
	public Optional<Cliente> consultarCPFCliente(String cpf) {
		
		try {
			Cliente cliente = restTemplate.getForObject("http://localhost:8081/"+cpf, Cliente.class);
			return Optional.of(cliente);
			
		}catch (HttpClientErrorException e) {
			logger.error(e.getMessage());
			return Optional.empty();
		}		
	}
	
	
}
